//Variables
const gulp = require("gulp");
const browserSync = require("browser-sync").create();
const sass = require("gulp-sass")(require("sass"));
const clean = require("gulp-clean");
const minify = require("gulp-js-minify");
const cleanCSS = require("gulp-clean-css");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");
const pipeline = require("readable-stream").pipeline;
const autoprefixer = require("gulp-autoprefixer");
const imagemin = require("gulp-imagemin");

//Dev task functions

function serve() {
  browserSync.init({
    server: {
      baseDir: "./",
    },
  });
}
exports.serve = serve;

function watchStyles() {
  gulp
    .watch("./src/scss/**/*.scss", buildStyles)
    .on("change", browserSync.reload);
}
exports.watchStyles = watchStyles;

function watchScripts() {
  gulp.watch("./src/js/*.js", buildScripts).on("change", browserSync.reload);
}

exports.watchScripts = watchScripts;

exports.dev = gulp.parallel(watchStyles, watchScripts, serve);
//Build task functions

function clear() {
  return gulp
    .src("./dist/*", {
      read: false,
    })
    .pipe(clean());
}

exports.clear = clear;

function buildStyles() {
  return gulp
    .src(["./src/scss/components/*.scss", "./src/scss/abstracts/*.scss"])

    .pipe(sass({ outputStyle: "compressed" }).on("error", sass.logError))
    .pipe(gulp.dest("./dist/css"));
}

exports.buildStyles = buildStyles;

exports.build = () =>
  gulp
    .src(["./src/scss/components/*.scss", "./src/scss/abstracts/*.scss"])
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(gulp.dest("./dist/css"));

function cleanCss() {
  return gulp
    .src("css/*.css")
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(gulp.dest("dist"));
}
exports.cleanCss = cleanCss;

function buildScripts() {
  return gulp.src("./src/js/*.js").pipe(gulp.dest("./dist/js"));
}

exports.buildScripts = buildScripts;

function minifyScripts() {
  return gulp
    .src("src/js/index.js", { allowEmpty: true })
    .pipe(minify({ noSource: true }))
    .pipe(gulp.dest("dist/js"));
}
exports.minifyScripts = minifyScripts;
//gulp.task('compress', function () {
//   return pipeline(
//         gulp.src('lib/*.js'),
//         uglify(),
//         gulp.dest('dist')
//   );
// });
function uglifyJs() {
  return pipeline(
    gulp.src("./src/js/index.js"),
    uglify(),
    gulp.dest("dist/js")
  );
}
exports.uglifyJs = uglifyJs;

function scriptConcat() {
  return gulp
    .src("./dist/js/*.js")
    .pipe(concat("all.js"))
    .pipe(gulp.dest("./dist/js"));
}
exports.scriptConcat = scriptConcat;

exports.build = () =>
  gulp.src("./src/img/*").pipe(imagemin()).pipe(gulp.dest("./dist/img"));

const moveImages = () => gulp.src("./src/img/*").pipe(gulp.dest("./dist/img/"));
exports.moveImages = moveImages;

exports.build = gulp.series(
  clear,
  buildStyles,
  cleanCss,
  scriptConcat,
  buildScripts,
  minifyScripts,
  moveImages
);
