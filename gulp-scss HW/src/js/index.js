// Header burger button
const burgerButton = document.querySelector(".header__burger");
const dropDownMenu = document.querySelector(".header__navigation");

burgerButton.addEventListener("click", () => {
  burgerButton.classList.toggle("open");
  dropDownMenu.classList.toggle("absolute");
});
